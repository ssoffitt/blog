from django.conf import settings as base_settings


def settings(request):
    return {'settings': base_settings}
