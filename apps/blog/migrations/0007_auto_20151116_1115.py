# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_delete_person'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='pub_date',
            field=models.DateTimeField(verbose_name=b'date published'),
        ),
        migrations.AlterField(
            model_name='note',
            name='text',
            field=models.TextField(),
        ),
    ]
