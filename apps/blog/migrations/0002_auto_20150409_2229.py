# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('title', models.CharField(max_length=100)),
                ('pub_date', models.DateTimeField()),
                ('text', models.CharField(max_length=5000)),
            ],
        ),
        migrations.DeleteModel(
            name='IndexModel',
        ),
    ]
