# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20150409_2229'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='pub_date',
        ),
        migrations.RemoveField(
            model_name='person',
            name='text',
        ),
        migrations.RemoveField(
            model_name='person',
            name='title',
        ),
    ]
