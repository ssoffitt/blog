# -*- encoding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import UserCreationForm as RegistrationForm
from .models import Note


class UserCreationForm(RegistrationForm):
    email = forms.EmailField(label=_(u'E-mail'))

    class Meta:
        model = User
        fields = (
            'username', 'email', 'password1', 'password2'
        )

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError(
                _(u'This email address is already taken. '
                  u'Please adjust your selection.'))

        return email

    def save(self, *args, **kwargs):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.save()
        return user


class AddNotesForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = (
            'title', 'text'
        )

    def clean_title(self):
        title = self.cleaned_data['title']
        if Note.objects.filter(title=title).exists():
            raise forms.ValidationError(
                _(u'This title is already taken. '))

        return title
