from django.conf.urls import url
from django.contrib.auth.views import password_change

from . import views


urlpatterns = [
    url(r'^$', views.discussions, name='discussions_page'),
    url(r'^register/$', views.registration, name='registration_page'),
    url(r'^discussions/(?P<title_id>[0-9]+)/$', views.detail_discussions,
        name='detail_discussions'),
    url(r'^main/$', views.main_page, name='main_page'),
    url(r'^home/$', views.home_page, name='home_page'),
    url(r'^profile/$', views.profile, name='profile_page'),
    url(r'^password_change/$', password_change, {
        'post_change_redirect': 'auth:password_change_done'},
        name='password_change'),
    url(r'^add_discussions/$',
        views.add_discussions, name='add_discussions_page')
]
