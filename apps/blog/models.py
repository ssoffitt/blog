from __future__ import unicode_literals
import datetime

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class Note(models.Model):
    author = models.ForeignKey('auth.User', verbose_name=_(u'Author'),
                               blank=True, null=True)
    title = models.CharField(max_length=100)
    pub_date = models.DateTimeField('date published', default=timezone.now)
    text = models.TextField()

    def __unicode__(self):
        return unicode(self.title)

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = _('Published recently?')

    @models.permalink
    def get_absolute_url(self):
        return 'blog:detail_discussions', (), {'title_id': self.pk}
