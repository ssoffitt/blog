from django.contrib import admin

# Register your models here.
from .models import Note


class NoteAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'text']}),
        ('Data information', {'fields': ['pub_date'], 'classes': ['collapse']})
    ]
    list_display = ('author', 'title', 'pub_date', 'text',
                    'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['title']

admin.site.register(Note, NoteAdmin)
