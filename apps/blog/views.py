from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.contrib.auth import login as login_user, authenticate
from django.shortcuts import get_object_or_404, render

from .forms import UserCreationForm, AddNotesForm
from .models import Note


def registration(request):
    form = UserCreationForm(request.POST or None)
    if form.is_valid():
        user = form.save()
        user = authenticate(username=user.username, password=user.password)
        if user and user.is_active:
            login_user(request, user)
        return redirect('blog:discussions_page')

    return render(
        request, 'registration/registration.html', {'form': form})


def main_page(request):
    return render(request, 'blog/main.html')


def detail_discussions(request, title_id):
    note = get_object_or_404(Note, pk=title_id)
    return render(request, 'blog/detail_discussion.html', {'note': note})


def discussions(request):
    latest_theme_list = Note.objects.filter(
        pub_date__lte=timezone.now()).order_by('-pub_date')[:5]
    context = {'latest_theme_list': latest_theme_list}
    return render(request, 'blog/discussions.html', context)


@login_required
def add_discussions(request):
    form = AddNotesForm(request.POST or None)
    if form.is_valid():
        post = form.save(commit=False)
        post.author = request.user
        post.pub_date = timezone.now()
        post.save()
        return redirect('blog:discussions_page')
    return render(request, 'blog/add_discussions.html', {'form': form})


@login_required
def profile(request):
    return render(request, 'blog/profile.html')


def home_page(request):
    return render(request, 'blog/home.html')
