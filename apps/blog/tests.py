import datetime

from django.utils import timezone
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

from .models import Note


class NoteMethodTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        """
        was_published_recently() should return False for note whose
        pub_date is in the future.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Note(pub_date=time)
        self.assertEqual(future_question.was_published_recently(), False)


def create_note(title, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Note.objects.create(title=title, pub_date=time)


class NoteViewTests(TestCase):
    def test_empty_notes(self):
        """
        Feature: test index view with no note
        """
        resp = self.client.get(reverse('blog:discussions_page'))
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'No notes are available.')
        self.assertEqual(Note.objects.count(), 0)

    def test_with_note(self):
        """
        Feature: test index view with note
        """
        create_note(title='Note.', days=-1)
        self.client.get(reverse('blog:discussions_page'))
        self.assertEqual(Note.objects.count(), 1)


class AuthTests(TestCase):
    def test_registration(self):
        """
        Feature: user registration
        1. Send user data to the server.
        2. Create an user.
        3. Log in an user.
        4. Redirect an user to the home page.
        """

        self.assertEqual(get_user_model().objects.count(), 0)
        self.client.post(reverse('blog:registration_page'), {
            'username': 'username',
            'email': 'test@mail.com',
            'password1': 'password',
            'password2': 'password'
        })
        self.assertEqual(get_user_model().objects.count(), 1)
        self.assertEqual(
            self.client.get(reverse('blog:discussions_page')).status_code, 200)

    def test_login(self):
        """
        Feature: user login
        1. Log in an user.
        2. Redirect an user to the home page.
        """

        self.client.login(username='username', password='password')
        self.assertEqual(
            self.client.get(reverse('blog:home_page')).status_code, 200)

    def test_logout(self):
        """
        Feature: user login
        1. Log out an user.
        2. Redirect an user to the login page.
        """

        self.client.logout()
        self.assertEqual(
            self.client.get(reverse('auth:login')).status_code, 200)


class TestRedirect(TestCase):
    """
    Feature: to redirect from the home page to the profile page
    """
    def test_redirect_to_profile(self):
        resp = self.client.get(reverse('blog:profile_page'))
        self.assertTemplateUsed(
            resp, 'blog/profile.html', 'blog/base.html')
