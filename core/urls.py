from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = [
    url(r'^blog/', include('apps.blog.urls', namespace='blog')),
    url(r'^auth/', include('django.contrib.auth.urls', namespace='auth')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^comments/', include('django_comments.urls')),
]
